#[derive(Debug, Copy, Clone)]
enum Color {
    Black,
    White,
}

#[derive(Debug, Copy, Clone)]
enum Piece {
    Rook(Color),
    Knight(Color),
    Bishop(Color),
    Queen(Color),
    King(Color),
    Pawn(Color),
}

fn is_empty(board: Board, pos: Position) -> bool {
    match board.0[pos.0][pos.1] {
        Some(_) => false,
        None => true,
    }
}

impl Piece {
    fn valid_move(&self, board: Board, from: Position, to: Position) -> bool {
        match self {
            Piece::Pawn(_) => self._valid_pawn_move(board, from, to),
            _ => true,
        }
    }

    fn _valid_pawn_move(&self, board: Board, from: Position, to: Position) -> bool {
        match from.1 {
            1 | 6 => {
                if from.0 == to.0 && to.1 <= from.1 + 2 && is_empty(b, to) {
                    return true
                }                
            },
            _ => {
                if from.0 == to.0 && to.1 == from.1 + 1  && is_empty(b, to) {
                    return true
                }
            }
        }

        false
    }
}

type Position = (usize, usize);

fn figures_row(c: Color) -> Vec<Option<Piece>> {
    vec![
        Some(Piece::Rook(c)),
        Some(Piece::Knight(c)),
        Some(Piece::Bishop(c)),
        Some(Piece::Queen(c)),
        Some(Piece::King(c)),
        Some(Piece::Bishop(c)),
        Some(Piece::Knight(c)),
        Some(Piece::Rook(c)),
    ]
}

fn pawns_row(c: Color) -> Vec<Option<Piece>> {
    (0..7)
        .map(|_| Some(Piece::Pawn(c)))
        .collect::<Vec<Option<Piece>>>()
}

fn empty_row() -> Vec<Option<Piece>> {
    (0..7).map(|_| None).collect::<Vec<Option<Piece>>>()
}

#[derive(Debug)]
struct Board(Vec<Vec<Option<Piece>>>);

impl Board {
    fn new() -> Board {
        Board(vec![
            figures_row(Color::White),
            pawns_row(Color::White),
            empty_row(),
            empty_row(),
            empty_row(),
            empty_row(),
            pawns_row(Color::Black),
            figures_row(Color::Black),
        ])
    }

    fn move_piece(self, from: Position, to: Position) -> Result<Board, String> {
        match self.0[from.0][from.1] {
            Some(p) => match p.valid_move(self, from, to) {
                true => {
                    self.0.[to.0].insert(p, to.0)
                    Ok(self)
                },
                false => Err(String::from("Invalid movement!")),
            }
            None => Err(String::from("No piece in selected position")),
        }
    }
}

// fn reducer(state: Board, action: ???) -> Board {

// }

fn main() {
    let board = Board::new();

    let res = board.move_piece((1, 1), (1, 5));

    println!("{:?}", res);
}
